package be.kdg.programming3;


import be.kdg.programming3.business.service.UpdatingListData;
import be.kdg.programming3.business.service.UpdatingListDataImp;
import be.kdg.programming3.presentation.View;
import be.kdg.programming3.repository.DataFacotry;
import be.kdg.programming3.repository.HardcodedDataFacotry;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.text.ParseException;

public class StartApplication {
    public static void main(String[] args) throws ParseException {

        SpringApplication.run(StartApplication.class, args);
        ConfigurableApplicationContext context = SpringApplication. run(Configuration.class,
                args);
        context.getBean(View.class).startMenu();
        context.close();
//        DataFacotry dataFacotry = new HardcodedDataFacotry();
//        UpdatingListData updatingListData = new UpdatingListDataImp(dataFacotry);
//        View view = new View(updatingListData);
//
//        view.startMenu();
    }
}