package be.kdg.programming3.presentation;

import be.kdg.programming3.JSonWriter;
import be.kdg.programming3.business.service.UpdatingListData;
import be.kdg.programming3.domain.Booking;
import be.kdg.programming3.domain.Show;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class View {
    private JSonWriter sJonWriter;
    Scanner scanner = new Scanner(System.in);

    private UpdatingListData updatingListData;

    public View(UpdatingListData updatingListData) {
        this.updatingListData = updatingListData;
    }

    //    private LocalDateSerializer localDateSerializer;

    private static void showMenu() {
        System.out.printf("""
                %nWelcome to our entertainment offers, what information are you looking for?
                0) Quit
                1) List of all available shows
                2) List of all available shows by amount of artists
                3) List of bookings
                4) List of bookings by date and / or location
                5) List of all Artist
                """);
    }

    public void startMenu(){

       showMenu();
        int choice = scanner.nextInt();


        while (choice != 0) {
            switch (choice) {

                case 1:
                    System.out.println(updatingListData.getShows());

                    JSonWriter.writeJson(updatingListData.getShows());
                    break;

                case 2:
                    System.out.println("How many artist are you looking for?");
                    int artistAmount = scanner.nextInt();

                    System.out.printf("Shows with %d artists:", artistAmount);
                    List<Show> ShowsByAmountOfArtists = updatingListData.getShows().stream().filter(show -> show.getArtists().size() == artistAmount).toList();
                    System.out.println(ShowsByAmountOfArtists);
                    JSonWriter.writeJson(ShowsByAmountOfArtists);
                    break;

                case 3:
                    System.out.println(updatingListData.getBookings());
                    JSonWriter.writeJson(updatingListData.getBookings());
                    break;

                case 4:
                    System.out.println("what date are you looking for? (dd-MM-yyyy e.g.: 23-06-1999). Leave blank to ignore ");
                    scanner.nextLine();

                    String dateInput = scanner.nextLine();

                    List<Booking> filtered = new ArrayList<>();

                    if (dateInput != ""){
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            Date date = simpleDateFormat.parse(dateInput);
                            System.out.println("You entered: " + simpleDateFormat.format(date));

                            List<Booking> bookingsByDate = updatingListData.getBookings().stream()
                                    .filter(booking -> booking.getBookingDate().toString().equals(date.toString())).toList();

                            filtered = bookingsByDate;

                            System.out.println(filtered);

                        } catch (ParseException e) {
                            System.err.println("Invalid date format. Please enter date in the format dd-MM-yyyy e.g.: 23-06-1999.");
                        }

                    }


                    System.out.println("what location are you looking for? You can enter part of a location.");

                    String locationInput = scanner.nextLine();

                    if (dateInput != ""){
                        filtered = filtered.stream()
                                .filter(booking -> booking.getLocation().toLowerCase().contains(locationInput.toLowerCase())).toList();
                    }
                    else{
                        filtered = updatingListData.getBookings().stream()
                                .filter(booking -> booking.getLocation().toLowerCase().contains(locationInput.toLowerCase())).toList();
                    }

                    System.out.println(filtered);
                    JSonWriter.writeJson(filtered);

                    break;

                case 5: System.out.println(updatingListData.getArtists());
                    JSonWriter.writeJson(updatingListData.getArtists());

                    break;


                default: System.out.printf("%d is not a valid choice, please enter your choice (0 - 5)",choice);
            }

            showMenu();

            try {
                choice = scanner.nextInt();
            } catch (java.util.InputMismatchException e) {
                System.err.println("Invalid input. Please enter a valid integer.");
                scanner.next();
            }

        }

    }

}

