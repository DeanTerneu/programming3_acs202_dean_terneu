package be.kdg.programming3;

import be.kdg.programming3.business.service.UpdatingListData;
import be.kdg.programming3.business.service.UpdatingListDataImp;
import be.kdg.programming3.presentation.View;
import be.kdg.programming3.repository.DataFacotry;
import be.kdg.programming3.repository.HardcodedDataFacotry;
import org.springframework.context.annotation.Bean;

public class Configuration {

    @Bean
    public DataFacotry dataFacotry(){
        return new HardcodedDataFacotry();
    }

    @Bean
    public UpdatingListData updatingListData(DataFacotry dataFacotry){
        return new UpdatingListDataImp(dataFacotry);
    }

    @Bean
    public View view(UpdatingListData updatingListData){
        return new View(updatingListData);
    };
}
