package be.kdg.programming3;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

//public abstract class LocalDateSerializer implements JsonSerializer<LocalDate> {
//        private static final DateTimeFormatter FORMATTER =
//                DateTimeFormatter.ofPattern("dd-MM-yyyy");
//        @Override
//        public JsonElement serialize(LocalDateTime localDate, Type typeOfSrc,
//                                     JsonSerializationContext context) {
//            return new JsonPrimitive(FORMATTER.format(localDate));
//        }
//}
