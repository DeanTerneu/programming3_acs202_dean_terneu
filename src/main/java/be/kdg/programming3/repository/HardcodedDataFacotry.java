package be.kdg.programming3.repository;

import be.kdg.programming3.domain.Artist;
import be.kdg.programming3.domain.Booking;
import be.kdg.programming3.domain.Show;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class HardcodedDataFacotry implements DataFacotry {

    private static List<Show> showList = new ArrayList<Show>();
    private static List<Artist> artistList = new ArrayList<Artist>();
    private static List<Booking> bookingList = new ArrayList<Booking>();

    static {
        try {
            seed();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    public static void seed() throws ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        Show show1 = new Show("Encontro",14,600, Show.Theme.ELEGANT);
        Show show2 = new Show("Tuakana",27,900, Show.Theme.URBAN);
        Show show3 = new Show("Mana O Te Ra",17,500, Show.Theme.URBAN);
        Show show4 = new Show("Fire-Eating",6,400, Show.Theme.BAROQUE);

        Artist artist1 = new Artist("Ines",dateFormat.parse("04-11-1995"),25);
        Artist artist2 = new Artist("Stijn",dateFormat.parse("07-05-1986"),6);
        Artist artist3 = new Artist("Pablo",dateFormat.parse("12-07-1993"),12);
        Artist artist4 = new Artist("Suzie",dateFormat.parse("24-10-1994"),9);

        Booking booking1 = new Booking(dateFormat.parse("24-01-2024"),false,"Berchem",show1);
        Booking booking2 = new Booking(dateFormat.parse("04-10-2023"),false,"Mechelen",show2);
        Booking booking3 = new Booking(dateFormat.parse("30-09-2023"),true,"Merksem",show3);
        Booking booking4 = new Booking(dateFormat.parse("25-12-2023"),true,"Luik",show4);
        Booking booking5 = new Booking(dateFormat.parse("12-12-2023"),true,"Dinant",show1);
        Booking booking6 = new Booking(dateFormat.parse("17-02-2024"),false,"Amsterdam",show3);
        Booking booking7 = new Booking(dateFormat.parse("22-07-2024"),false,"Brussel",show4);

        show1.addAritst(artist1);
        show1.addAritst(artist3);
        booking1.addArist(artist1);
        booking1.addArist(artist3);
        booking5.addArist(artist1);
        booking5.addArist(artist3);
        artist1.addShow(show1);
        artist3.addShow(show1);
        show1.addBooking(booking1);
        show1.addBooking(booking5);
        artist1.addBooking(booking1);
        artist1.addBooking(booking5);
        artist3.addBooking(booking1);
        artist3.addBooking(booking5);


        show2.addAritst(artist2);
        show2.addAritst(artist3);
        booking2.addArist(artist2);
        booking2.addArist(artist3);
        artist2.addShow(show2);
        artist3.addShow(show2);
        show2.addBooking(booking2);
        artist2.addBooking(booking2);
        artist3.addBooking(booking2);


        show3.addAritst(artist3);
        booking3.addArist(artist3);
        booking5.addArist(artist3);
        artist3.addShow(show3);
        show3.addBooking(booking3);
        show3.addBooking(booking6);
        artist3.addBooking(booking3);
        artist3.addBooking(booking6);


        show4.addAritst(artist3);
        show4.addAritst(artist4);
        booking4.addArist(artist3);
        booking7.addArist(artist4);
        booking4.addArist(artist4);
        booking7.addArist(artist3);
        artist3.addShow(show4);
        artist4.addShow(show4);
        show4.addBooking(booking4);
        show4.addBooking(booking7);
        artist3.addBooking(booking4);
        artist3.addBooking(booking7);
        artist4.addBooking(booking4);
        artist4.addBooking(booking7);

        getShowListStatic().add(show1);
        getShowListStatic().add(show2);
        getShowListStatic().add(show3);
        getShowListStatic().add(show4);

        getArtistListStatic().add(artist1);
        getArtistListStatic().add(artist2);
        getArtistListStatic().add(artist3);
        getArtistListStatic().add(artist4);

        getBookingListStatic().add(booking1);
        getBookingListStatic().add(booking2);
        getBookingListStatic().add(booking3);
        getBookingListStatic().add(booking4);
    }

    public static List<Show> getShowListStatic() {
        return showList;
    }

    public static List<Artist> getArtistListStatic() {
        return artistList;
    }

    public static List<Booking> getBookingListStatic() {
        return bookingList;
    }

    @Override
    public  List<Show> readShowList() {
        return showList;
    }
    @Override
    public  List<Artist> readArtistList() {
        return artistList;
    }
    @Override
    public  List<Booking> readBookingList() {
        return bookingList;
    }
}
