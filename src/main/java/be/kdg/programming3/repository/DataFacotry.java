package be.kdg.programming3.repository;

import be.kdg.programming3.domain.Artist;
import be.kdg.programming3.domain.Booking;
import be.kdg.programming3.domain.Show;

import java.util.List;

public interface DataFacotry {

    List<Show> readShowList();

    List<Artist> readArtistList();

    List<Booking> readBookingList();
}
