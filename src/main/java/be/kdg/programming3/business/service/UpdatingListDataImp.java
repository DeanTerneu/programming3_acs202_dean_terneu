package be.kdg.programming3.business.service;

import be.kdg.programming3.domain.Artist;
import be.kdg.programming3.domain.Booking;
import be.kdg.programming3.domain.Show;
import be.kdg.programming3.repository.DataFacotry;

import java.util.Date;
import java.util.List;

public class UpdatingListDataImp implements UpdatingListData {

    private DataFacotry dataFacotry;

    public UpdatingListDataImp(DataFacotry dataFacotry) {
        this.dataFacotry = dataFacotry;
    }


    @Override
    public void createShow(String name, double duration, double price, Show.Theme theme) {
        Show show = new Show(name,duration,price,theme);
        addShow(show);
    }

    @Override
    public void createArtist(String name, Date date, int yearsOfExperience){
        Artist artist = new Artist(name,date,yearsOfExperience);
        addArtist(artist);
    }

    @Override
    public void createBooking(Date date, boolean isPaid, String location, Show show){

        if (!dataFacotry.readShowList().contains(show)){
            System.err.println("show does not exist");
        }
        else{
            Booking booking = new Booking(date,isPaid,location,show);
            addBooking(booking);
        }
    }

    @Override
    public List<Booking> getBookings(){
        return dataFacotry.readBookingList();
    }
    @Override
    public List<Artist> getArtists(){
        return dataFacotry.readArtistList();
    }
    @Override
    public List<Show> getShows(){
        return dataFacotry.readShowList();
    }

    @Override
    public void addBooking(Booking booking) {
        if (!dataFacotry.readBookingList().contains(booking)) {
            dataFacotry.readBookingList().add(booking);
        } else {}
    }

    @Override
    public void removeBooking(Booking booking) {
        if (dataFacotry.readBookingList().contains(booking)) {
            dataFacotry.readBookingList().remove(booking);
        } else {}
    }

    @Override
    public void addShow(Show show) {
        if (!dataFacotry.readShowList().contains(show)){
            dataFacotry.readShowList().add(show);
        }
        else{}
    }

    @Override
    public void removeShow(Show show){
        if (dataFacotry.readShowList().contains(show)){
            dataFacotry.readShowList().remove(show);
        }
        else{}
    }

    @Override
    public void addArtist(Artist artist){
        if(!dataFacotry.readArtistList().contains(artist)){
            dataFacotry.readArtistList().add(artist);
        }
        else{}
    }

    @Override
    public void removeArtist(Artist artist){
        if(dataFacotry.readArtistList().contains(artist)){
            dataFacotry.readArtistList().remove(artist);
        }
        else{}
    }
}


