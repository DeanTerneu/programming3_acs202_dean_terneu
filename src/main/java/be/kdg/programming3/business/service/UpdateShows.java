package be.kdg.programming3.business.service;

import be.kdg.programming3.domain.Artist;
import be.kdg.programming3.domain.Booking;
import be.kdg.programming3.domain.Show;

import java.util.Date;
import java.util.List;

public class UpdateShows implements UpdatingListData{


    @Override
    public void createShow(String name, double duration, double price, Show.Theme theme) {

    }

    @Override
    public void createArtist(String name, Date date, int yearsOfExperience) {

    }

    @Override
    public void createBooking(Date date, boolean isPaid, String location, Show show) {

    }

    @Override
    public void addBooking(Booking booking) {

    }

    @Override
    public void removeBooking(Booking booking) {

    }

    @Override
    public void addShow(Show show) {

    }

    @Override
    public void removeShow(Show show) {

    }

    @Override
    public void addArtist(Artist artist) {

    }

    @Override
    public void removeArtist(Artist artist) {

    }

    @Override
    public List<Booking> getBookings() {
        return null;
    }

    @Override
    public List<Artist> getArtists() {
        return null;
    }

    @Override
    public List<Show> getShows() {
        return null;
    }
}
