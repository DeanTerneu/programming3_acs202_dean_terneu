package be.kdg.programming3.business.service;

import be.kdg.programming3.domain.Artist;
import be.kdg.programming3.domain.Booking;
import be.kdg.programming3.domain.Show;

import java.util.Date;
import java.util.List;

public interface UpdatingListData {
    void createShow(String name, double duration, double price, Show.Theme theme);

    void createArtist(String name, Date date, int yearsOfExperience);

    void createBooking(Date date, boolean isPaid, String location, Show show);

    void addBooking(Booking booking);

    void removeBooking(Booking booking);

    void addShow(Show show);

    void removeShow(Show show);

    void addArtist(Artist artist);

    void removeArtist(Artist artist);

    List<Booking> getBookings();


    List<Artist> getArtists();

    List<Show> getShows();
}
