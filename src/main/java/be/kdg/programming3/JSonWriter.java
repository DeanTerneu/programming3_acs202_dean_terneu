package be.kdg.programming3;

import be.kdg.programming3.domain.Artist;
import be.kdg.programming3.domain.Booking;
import be.kdg.programming3.domain.Show;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JSonWriter {
    public static final String SHOWS_JSON = "SHOWS.json";
    public static final String ARTISTS_JSON = "ARTISTS.json";
    public static final String BOOKINGS_JSON = "BOOKINGS.json";


    public static final void writeJson(List<?> list) {

        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting(); //prints it beautifully
        Gson gson = builder.create();
//        builder.registerTypeAdapter(LocalDate.class,
//                new LocalDateSerializer() {
//                    @Override
//                    public JsonElement serialize(LocalDate src, Type typeOfSrc, JsonSerializationContext context) {
//                        return null;
//                    }
//                });
        String jsonString = gson.toJson(list);
        System.out.println(jsonString);

        if (!list.isEmpty()) {
            Object firstItem = list.get(0);

            if (firstItem instanceof Show) {
                writeShows(jsonString);
            } else if (firstItem instanceof Artist) {
                writeArtist(jsonString);
            } else if (firstItem instanceof Booking) {
                writeBookings(jsonString);
            } else {
                System.out.println("Unknown type in the list");
            }
        } else {
            System.out.println("List is empty");
        }
    }


    private static void writeShows(String jsonString) {
        try (FileWriter jsonWriter = new FileWriter(SHOWS_JSON)) {
            jsonWriter.write(jsonString);
        } catch (IOException e) {
            System.err.println("error writing to file" + SHOWS_JSON);
            e.printStackTrace();
        }
    }

    private static void writeArtist(String jsonString) {
        try (FileWriter jsonWriter = new FileWriter(ARTISTS_JSON)) {
            jsonWriter.write(jsonString);
        } catch (IOException e) {
            System.err.println("error writing to file" + ARTISTS_JSON);
            e.printStackTrace();
        }
    }

    private static void writeBookings(String jsonString) {
        try (FileWriter jsonWriter = new FileWriter(BOOKINGS_JSON)) {
            jsonWriter.write(jsonString);
        } catch (IOException e) {
            System.err.println("error writing to file" + BOOKINGS_JSON);
            e.printStackTrace();
        }
    }
}


