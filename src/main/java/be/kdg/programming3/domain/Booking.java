package be.kdg.programming3.domain;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Booking {

    private Date bookingDate ;
    private boolean paid;
    private String location;
    private Show show;
    private transient List<Artist> artists;

    public Booking (Date bookingDate, boolean paid, String location, Show show) {

        this.bookingDate = bookingDate;
        this.paid = paid;
        this.location = location;
        this.show = show;
        this.artists = new ArrayList<>();
    }

//    @Override
//    public String toString() {
//        return String.format("Date: %2s%nShow: %s%nPaid: %b%nLocation: %s%nArtists: %s%n",
//                getBookingDate(),getShow(),isPaid(),getLocation(),getArtists());
//
//    }

    @Override
    public String toString() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = dateFormat.format(getBookingDate());

        return String.format("Date: %2s%nPaid: %b%nLocation: %s%n",
                formattedDate,isPaid(),getLocation());

    }

    public void addArist(Artist artist) {
        if (artists.contains(artist)){
        }
        else{ artists.add(artist);}
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public boolean isPaid() {
        return paid;
    }

    public String getLocation() {
        return location;
    }

    public Show getShow() {
        return show;
    }

    public List<Artist> getArtists() {
        return artists;
    }


}
