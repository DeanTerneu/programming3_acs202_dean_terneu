package be.kdg.programming3.domain;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Artist {

    private String name;
    private Date DOB;
    private int yearsOfExperience;
    private transient List<Show> shows;
    private transient List<Booking> bookings;

    public Artist(String name, Date DOB, int yearsOfExperience) {
        this.name = name;
        this.DOB = DOB;
        this.yearsOfExperience = yearsOfExperience;
        this.shows = new ArrayList<>();
        this.bookings = new ArrayList<>();
    }

    @Override
//    public String toString() {
//        return String.format("Name: %s%nDOB: %s%nYears of experience: %d%nShows: %s%nBookings %s%n",
//                getName(),getDOB(),getYearsOfExperience(),shows,bookings);
//
//    }

    public String toString() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = dateFormat.format(getDOB());

        return String.format("Name: %s%nDOB: %s%nYears of experience: %d%n",
                getName(),formattedDate,getYearsOfExperience());

    }

    public void addShow(Show show){
        if (shows.contains(show)){
        }
        else{ shows.add(show);}
    }

    public void addBooking(Booking booking){
        if (bookings.contains(booking)){
        }
        else{ bookings.add(booking);}
    }

    public String getName() {
        return name;
    }

    public Date getDOB() {
        return DOB;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public List<Show> getShows() {
        return shows;
    }

    public List<Booking> getBookings() {
        return bookings;
    }

}

