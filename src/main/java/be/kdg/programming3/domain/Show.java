package be.kdg.programming3.domain;

import java.util.ArrayList;
import java.util.List;

public class Show {

    private String name;
    private double duration;
    private double price;
    private Theme theme;
    private transient List<Artist> artists;
    private transient List<Booking> bookings;

    public enum Theme{
        ELEGANT,
        MADMAX,
        URBAN,
        BAROQUE
    }

    public Show(String name, double duration, double price, Theme theme) {
        this.name = name;
        this.duration = duration;
        this.price = price;
        this.theme = theme;
        this.artists = new ArrayList<>();
        this.bookings = new ArrayList<>();
    }


    @Override
    public String toString() {
        return String.format("Show: %s%nDuration: %.2f%nPrice: %.2f%nTheme: %s%nArtists: %s%nBookings %s%n",
                getName(),getDuration(),getPrice(),theme.name(),artists,bookings);

    }

    public void addAritst(Artist artist) {
        if (artists.contains(artist)){
        }
        else{ artists.add(artist);}
    }

    public void addBooking(Booking booking){
        if (bookings.contains(booking)){
        }
        else{ bookings.add(booking);}
    }

    public String getName() {
        return name;
    }

    public double getDuration() {
        return duration;
    }

    public double getPrice() {
        return price;
    }

    public Theme getTheme() {
        return theme;
    }

    public List<Artist> getArtists() {
        return artists;
    }

    public List<Booking> getBookings() {
        return bookings;
    }
}

